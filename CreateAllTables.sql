USE [MCRISS2-APP-DEV]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDistrict]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create     FUNCTION [dbo].[fn_GetDistrict](@inputOrgId INT) 
returns VARCHAR(250) 
AS 
  BEGIN 
		declare @returnDistName varchar(255)
		declare @distTypeEnumId int = (SELECT enumid FROM [enumeration] WHERE typeid = 16 AND code = 'D');
with tree as
(
       --set the anchor or start point of the recursive loop
       select organizationid, parentid, organizationtypeenum, [name] from organization where organizationid = @inputOrgId
       union all
       --join on the cte parentid and the search table's primary id
       select p.organizationid, p.parentid, p.organizationtypeenum, p.[name] from organization p
       inner join tree c on c.parentid = p.organizationid
       --select from the resulting tree
)
select @returnDistName = [name] from tree where organizationtypeenum = @distTypeEnumId



      RETURN @returnDistName 
  END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetRegion]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create     FUNCTION [dbo].[fn_GetRegion](@inputOrgId INT) 
returns VARCHAR(250) 
AS 
  BEGIN 
		declare @returnOrgName varchar(255)
		declare @regionTypeEnumId int = (SELECT enumid FROM [enumeration] WHERE typeid = 16 AND code = 'R');
with tree as
(
       --set the anchor or start point of the recursive loop
       select organizationid, parentid, organizationtypeenum, [name] from organization where organizationid = @inputOrgId
       union all
       --join on the cte parentid and the search table's primary id
       select p.organizationid, p.parentid, p.organizationtypeenum, p.[name] from organization p
       inner join tree c on c.parentid = p.organizationid
       --select from the resulting tree
)
select @returnOrgName = [name] from tree where organizationtypeenum = @regionTypeEnumId



      RETURN @returnOrgName 
  END;
GO
/****** Object:  Table [dbo].[Billet]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Billet](
	[BilletId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[IsProduction] [bit] NOT NULL,
	[MosEnum] [int] NOT NULL,
	[SectionEnum] [int] NULL,
	[Name] [varchar](90) NOT NULL,
	[ShortName] [varchar](45) NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Billet] PRIMARY KEY CLUSTERED 
(
	[BilletId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enumeration]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enumeration](
	[EnumId] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Parent] [int] NULL,
	[Code] [varchar](30) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [pk_Enumeration] PRIMARY KEY CLUSTERED 
(
	[EnumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization](
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[MepsOrganizationId] [int] NULL,
	[OrganizationTypeEnum] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[Name] [varchar](90) NOT NULL,
	[ShortName] [varchar](45) NULL,
	[Mcc] [varchar](5) NOT NULL,
	[Ruc] [varchar](5) NOT NULL,
	[ParentMcc] [varchar](5) NOT NULL,
	[ParentRuc] [varchar](5) NOT NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Organization] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_Billet]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   view [dbo].[vw_Billet] as 
select 
	B.BilletId,
	B.IsProduction,
	B.MOSEnum,
	B.Name as 'BilletName',
	B.OrganizationId,
	O.Name as 'OrganizationName',
	dbo.fn_GetDistrict(B.OrganizationId) as 'District',
	dbo.fn_GetRegion(B.OrganizationId) as 'Region',
	E.Description,
	O.OrganizationTypeEnum,
	E2.Description as 'UnitType',
	E3.Description as 'Section'
 from Billet B
 left Join Organization O on O.OrganizationId = B.OrganizationId
 left Join Enumeration E on E.EnumId = B.MOSEnum
 left join Enumeration E2 on E2.EnumId = O.OrganizationTypeEnum
 left join Enumeration E3 on E3.EnumId = B.SectionEnum
GO
/****** Object:  Table [dbo].[CourseCatalog]    Script Date: 5/17/2018 10:36:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseCatalog](
	[CourseCatalogId] [int] IDENTITY(1,1) NOT NULL,
	[CourseId] [int] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[BilletId] [int] NOT NULL,
	[CourseTypeEnum] [int] NOT NULL,
	[CourseOwner] [varchar](90) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[CourseNumber] [varchar](90) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_CourseCatalog] PRIMARY KEY CLUSTERED 
(
	[CourseCatalogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](90) NOT NULL,
	[ShortName] [varchar](90) NULL,
	[OrganizationTypeEnum] [int] NOT NULL,
	[BilletSectionTypeEnum] [int] NOT NULL,
	[Certification] [varchar](90) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Course] PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_Course]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   view [dbo].[vw_Course] as 
select	CC.CourseId,
		CC.CourseNumber,
		CC.Description,
		C.Name as 'Course Name',
		E1.Description as 'Billet Type',
		CC.StartDate,
		dbo.fn_GetDistrict(cc.OrganizationId) as 'District',
		dbo.fn_GetRegion(cc.OrganizationId) as 'Region',
		E2.Description as 'Course Type'
 from CourseCatalog CC
 left Join Course C on C.CourseId = CC.CourseId
 left join Enumeration E1 on E1.EnumId = C.BilletSectionTypeEnum
 left join Enumeration E2 on E2.EnumId = CC.CourseTypeEnum
GO
/****** Object:  Table [dbo].[Person]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[Dodidn] [varchar](10) NULL,
	[Ssn] [int] NOT NULL,
	[PersonTypeEnum] [int] NOT NULL,
	[FirstName] [varchar](15) NOT NULL,
	[MiddleName] [varchar](25) NULL,
	[LastName] [varchar](25) NOT NULL,
	[MaidenName] [varchar](25) NULL,
	[SuffixesEnum] [int] NULL,
	[Gender] [varchar](1) NOT NULL,
	[EthnicityEnum] [int] NULL,
	[IsHispanic] [bit] NOT NULL,
	[RaceEnum] [int] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[MaritalEnum] [int] NOT NULL,
	[Height] [decimal](5, 2) NOT NULL,
	[Weight] [decimal](5, 2) NOT NULL,
	[PersonalPhone] [varchar](17) NOT NULL,
	[AlternativePhone] [varchar](17) NULL,
	[PersonalEmail] [varchar](90) NOT NULL,
	[AlternativeEmail] [varchar](90) NULL,
	[UserName] [varchar](45) NOT NULL,
	[DriverLicNum] [varchar](45) NOT NULL,
	[DriverLicStateEnum] [int] NOT NULL,
	[DriverLicExpDate] [date] NOT NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Person] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recruiter]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recruiter](
	[RecruiterId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[BilletId] [int] NOT NULL,
	[IsSingleParent] [bit] NOT NULL,
	[IsInEfmp] [bit] NOT NULL,
	[MosEnum] [int] NOT NULL,
	[RankEnum] [int] NOT NULL,
	[EadRecruiter] [bit] NULL,
	[RankDate] [date] NOT NULL,
	[SecurityClearance] [varchar](10) NOT NULL,
	[ClearanceExpDate] [date] NOT NULL,
	[EfmpRemarks] [varchar](100) NOT NULL,
	[HasActiveDutySpouse] [bit] NOT NULL,
	[AciveDutySpouseBranchEnum] [int] NULL,
	[FMcc] [varchar](5) NOT NULL,
	[FRuc] [varchar](5) NOT NULL,
	[FMccDescription] [varchar](255) NOT NULL,
	[Mcc] [varchar](5) NOT NULL,
	[Ruc] [varchar](5) NOT NULL,
	[MccDescription] [varchar](255) NOT NULL,
	[EccDate] [date] NOT NULL,
	[EasDate] [date] NOT NULL,
	[CustodyStatus] [varchar](255) NOT NULL,
	[HasNJPorCourtMartial] [bit] NOT NULL,
	[NJPorCourtMartialDate] [date] NULL,
	[HasNegPage11Counseling] [bit] NOT NULL,
	[NegPage11Counseling] [varchar](1000) NULL,
	[HasDrugIncident] [bit] NOT NULL,
	[DrugIncident] [varchar](1000) NULL,
	[HasTreatForMenEmoPsyProb] [bit] NOT NULL,
	[TreatForMenEmoPsyProb] [varchar](1000) NULL,
	[HasMedicalProblemEffect] [bit] NOT NULL,
	[MedicalProbroblemEffect] [varchar](1000) NULL,
	[HasCounseling] [bit] NOT NULL,
	[TypeCounseling] [varchar](1000) NULL,
	[HasBeenPlaceOnMedication] [bit] NOT NULL,
	[MedicationList] [varchar](1000) NULL,
	[HasSpeechImpediments] [bit] NOT NULL,
	[SpeechImpediments] [varchar](1000) NULL,
	[HasPoliceInvolve] [bit] NOT NULL,
	[TypePoliceInvolve] [varchar](1000) NULL,
	[IsUnder26DriverImproveCard] [bit] NOT NULL,
	[Under26DriverImproveCard] [varchar](1000) NULL,
	[HasDepentNotGoToRS] [bit] NOT NULL,
	[DepenNotGoToRS] [varchar](1000) NULL,
	[HasFinancialInstabilities] [bit] NOT NULL,
	[TypeFinancialInstabilities] [varchar](1000) NULL,
	[HasIssueEffectAssignment] [bit] NOT NULL,
	[IssueEffectAssignment] [varchar](1000) NULL,
	[IsStatusChangeAtArrival] [bit] NOT NULL,
	[StatusChangeAtArrival] [varchar](1000) NULL,
	[DateCurrentTourBegan] [date] NOT NULL,
	[ArmedForcesActDutyBase] [date] NOT NULL,
	[EducationLevel] [varchar](50) NOT NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Recruiter] PRIMARY KEY CLUSTERED 
(
	[RecruiterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_RecruiterPerson]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_RecruiterPerson] as 
  SELECT [Person].[Dodidn]
		,[Recruiter].[RecruiterId]
		,[Recruiter].[RankEnum]
		,[Person].[Gender] 
  FROM [MCRISS2-APP-DEV].[dbo].[Recruiter] 
  INNER JOIN [MCRISS2-APP-DEV].[dbo].[Person]
	ON [Recruiter].[PersonId] = [Person].[PersonId]
GO
/****** Object:  View [dbo].[vw_Organization]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   view [dbo].[vw_Organization] as 
select Org.OrganizationId, 
		Org.OrganizationTypeEnum as TypeId,
		Org.Mcc,
		Org.Name,
		Enum.Description as OrganizationType,
		dbo.fn_GetRegion(Org.OrganizationId) as 'Region',
		dbo.fn_GetDistrict(Org.OrganizationId) as 'District'
 from dbo.Organization as Org
 left Join dbo.Enumeration as Enum on Enum.EnumId = OrganizationTypeEnum;
GO
/****** Object:  Table [dbo].[Address]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[AddressTypeEnum] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[Street] [varchar](29) NOT NULL,
	[Street2] [varchar](29) NULL,
	[City] [varchar](21) NOT NULL,
	[StateEnum] [int] NOT NULL,
	[ZipCodeEnum] [int] NOT NULL,
	[ZipPlusFour] [varchar](4) NULL,
	[CountyEnum] [int] NULL,
	[CountryEnum] [int] NULL,
	[RFMIS] [varchar](90) NULL,
	[SuiteBldgNumber] [varchar](45) NULL,
	[LocalInOptionEnum] [int] NULL,
	[InitialOperatCapability] [date] NULL,
	[ManningLevel] [int] NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[AuthorizedGovtVehicle] [int] NULL,
 CONSTRAINT [pk_Address] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignmentSheet8411]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignmentSheet8411](
	[AssignmentSheetId] [int] IDENTITY(1,1) NOT NULL,
	[RecruiterId] [int] NOT NULL,
	[BackgroundEnum] [int] NOT NULL,
	[AmountCleared] [decimal](18, 2) NOT NULL,
	[HometownCity] [varchar](21) NOT NULL,
	[HometownStateEnum] [int] NOT NULL,
	[HometownCitySpouse] [varchar](21) NULL,
	[HometownStateSpouseEnum] [int] NULL,
	[FormerRecruiter] [bit] NOT NULL,
	[HasSelectedPromotion] [bit] NOT NULL,
	[Volunteer] [bit] NOT NULL,
	[MarriageInterracial] [bit] NULL,
	[SpouseRaceEnum] [int] NULL,
	[OwnHome] [bit] NOT NULL,
	[OwnHomeCity] [varchar](21) NULL,
	[OwnHomeStateEnum] [int] NULL,
	[HasMedicalProblem] [bit] NOT NULL,
	[MedicalProblems] [varchar](1000) NULL,
	[HasUniqueDutyAssignment] [bit] NOT NULL,
	[UniqueCircumDutyAssignment] [varchar](1000) NULL,
	[PropertyType] [varchar](10) NOT NULL,
	[PlanRentOrBuy] [varchar](4) NOT NULL,
	[NumberBedrooms] [int] NOT NULL,
	[Contents] [varchar](12) NOT NULL,
	[DaysPlannedLeave] [int] NOT NULL,
	[FamilyAtRS] [bit] NOT NULL,
	[EstDateArrivalRS] [date] NOT NULL,
	[PresentFamilyLocCity] [varchar](21) NOT NULL,
	[PresentFamilyLocStateEnum] [int] NOT NULL,
	[1stDistPrefChoice] [int] NOT NULL,
	[2ndDistPrefChoice] [int] NOT NULL,
	[3rdDistPrefChoice] [int] NOT NULL,
	[1stDist1stRSChoice] [int] NOT NULL,
	[1stDist2ndRSChoice] [int] NOT NULL,
	[4thDist1stRSChoice] [int] NOT NULL,
	[4thDist2ndRSChoice] [int] NOT NULL,
	[6thDist1stRSChoice] [int] NOT NULL,
	[6thDist2ndRSChoice] [int] NOT NULL,
	[8thDist1stRSChoice] [int] NOT NULL,
	[8thDist2ndRSChoice] [int] NOT NULL,
	[9thDist1stRSChoice] [int] NOT NULL,
	[9thDist2ndRSChoice] [int] NOT NULL,
	[12thDist1stRSChoice] [int] NOT NULL,
	[12thDist2ndRSChoice] [int] NOT NULL,
	[RemarksForAssignedDist] [varchar](1000) NULL,
	[AssignedDistrict] [varchar](255) NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_AssignmentSheet8411] PRIMARY KEY CLUSTERED 
(
	[AssignmentSheetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Asvab]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asvab](
	[AsvabId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AnswerSheetNumber] [varchar](10) NOT NULL,
	[TestSite] [varchar](10) NOT NULL,
	[MetSiteId] [varchar](10) NOT NULL,
	[AdminIdentifier] [int] NOT NULL,
	[HighSchoolId] [int] NULL,
	[StudyCode] [int] NOT NULL,
	[TestSource] [int] NOT NULL,
	[AoScore] [int] NOT NULL,
	[AsvabAsp] [varchar](10) NOT NULL,
	[AfqtScore] [int] NOT NULL,
	[ArSubtest] [int] NOT NULL,
	[AsvabDate] [datetime] NOT NULL,
	[AsSubtest] [int] NOT NULL,
	[ClScore] [int] NOT NULL,
	[CsSubtest] [int] NOT NULL,
	[EiSubtest] [int] NOT NULL,
	[ElScore] [int] NOT NULL,
	[GsSubtest] [int] NOT NULL,
	[McSubtest] [int] NOT NULL,
	[MkSubtest] [int] NOT NULL,
	[MmScore] [int] NOT NULL,
	[NoSubtest] [int] NOT NULL,
	[PcSubtest] [int] NOT NULL,
	[RetestEligibilityDate] [date] NOT NULL,
	[VersionEnum] [int] NOT NULL,
	[VeSubtest] [int] NOT NULL,
	[WkSubtest] [int] NOT NULL,
 CONSTRAINT [pk_Asvab] PRIMARY KEY CLUSTERED 
(
	[AsvabId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BilletHistoryCriteria]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BilletHistoryCriteria](
	[BilletId] [int] NOT NULL,
	[RequiredBilletId] [int] NOT NULL,
 CONSTRAINT [pk_BilletHistoryCriteria] PRIMARY KEY CLUSTERED 
(
	[BilletId] ASC,
	[RequiredBilletId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BilletSpecialCriteria]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BilletSpecialCriteria](
	[BilletId] [int] NOT NULL,
	[RaceEnum] [int] NULL,
	[EthnicityEnum] [int] NULL,
	[LanguageEnum] [int] NULL,
	[Gender] [varchar](1) NULL,
 CONSTRAINT [pk_BilletSpecialCriteria] PRIMARY KEY CLUSTERED 
(
	[BilletId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ByNameRequest]    Script Date: 5/17/2018 10:36:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ByNameRequest](
	[ByNameRequestId] [int] IDENTITY(1,1) NOT NULL,
	[RequestedId] [int] NOT NULL,
	[RequesterId] [int] NOT NULL,
	[RequestedDate] [date] NOT NULL,
 CONSTRAINT [pk_ByNameRequest] PRIMARY KEY CLUSTERED 
(
	[ByNameRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dependent]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dependent](
	[DependentId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[DependentTypeEnum] [int] NOT NULL,
	[Gender] [varchar](1) NULL,
	[Age] [int] NOT NULL,
	[CreatedBy] [varchar](90) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](90) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [pk_Dependent] PRIMARY KEY CLUSTERED 
(
	[DependentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EnumerationCategory]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnumerationCategory](
	[EnumCatId] [int] IDENTITY(1,1) NOT NULL,
	[EnumTypeId] [int] NOT NULL,
	[Category] [varchar](45) NOT NULL,
 CONSTRAINT [pk_EnumerationCategory] PRIMARY KEY CLUSTERED 
(
	[EnumCatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupInstructor]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupInstructor](
	[GroupInstructorId] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[InstructorId] [int] NOT NULL,
 CONSTRAINT [pk_GroupInstructor] PRIMARY KEY CLUSTERED 
(
	[GroupInstructorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupRoster]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupRoster](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[CourseCatalogId] [int] NOT NULL,
	[ClassCode] [int] NOT NULL,
	[GroupNumber] [int] NOT NULL,
 CONSTRAINT [pk_GroupRoster] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPM_HelpMaterials]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPM_HelpMaterials](
	[helpId] [int] IDENTITY(1,1) NOT NULL,
	[helpType] [varchar](255) NULL,
	[helpName] [varchar](255) NULL,
	[helpDescription] [varchar](4000) NULL,
	[docId] [int] NULL,
	[url] [varchar](255) NULL,
	[helpText] [varchar](4000) NULL,
	[activeInd] [int] NULL,
	[createdBy] [varchar](255) NULL,
	[createdDate] [date] NULL,
 CONSTRAINT [pk_MPM_HelpMaterials] PRIMARY KEY CLUSTERED 
(
	[helpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPM_HMRevisionHistory]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPM_HMRevisionHistory](
	[versionId] [int] IDENTITY(1,1) NOT NULL,
	[helpId] [int] NOT NULL,
	[modifiedBy] [varchar](255) NULL,
	[modifiedDate] [date] NULL,
 CONSTRAINT [pk_MPM_HMRevisionHistory] PRIMARY KEY CLUSTERED 
(
	[versionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPM_HMRoles]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPM_HMRoles](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[helpId] [int] NOT NULL,
	[roleName] [varchar](255) NULL,
	[modifiedBy] [varchar](255) NULL,
	[modifiedDate] [date] NULL,
 CONSTRAINT [pk_MPM_HMRoles] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganizationAddress]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganizationAddress](
	[OrganizationAddressId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [pk_OrganizationAddress] PRIMARY KEY CLUSTERED 
(
	[OrganizationAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonAddress]    Script Date: 5/17/2018 10:36:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonAddress](
	[PersonAddressId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [pk_PersonAddress] PRIMARY KEY CLUSTERED 
(
	[PersonAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonLanguage]    Script Date: 5/17/2018 10:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonLanguage](
	[PersonLanguageId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[LanguageEnum] [int] NOT NULL,
 CONSTRAINT [pk_PersonLanguage] PRIMARY KEY CLUSTERED 
(
	[PersonLanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecruiterBilletHistory]    Script Date: 5/17/2018 10:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecruiterBilletHistory](
	[RecruiterBilletHistid] [int] IDENTITY(1,1) NOT NULL,
	[RecruiterId] [int] NOT NULL,
	[BilletId] [int] NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
 CONSTRAINT [pk_RecruiterBilletHistory] PRIMARY KEY CLUSTERED 
(
	[RecruiterBilletHistid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecruiterClassStatus]    Script Date: 5/17/2018 10:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecruiterClassStatus](
	[RecruiterClassStatusId] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[RecruiterId] [int] NOT NULL,
	[GraduationDate] [date] NULL,
	[DropReasonEnum] [int] NULL,
	[DropDate] [date] NULL,
	[Remarks] [varchar](255) NULL,
	[CheckInDate] [date] NULL,
 CONSTRAINT [pk_RecruiterClassStatus] PRIMARY KEY CLUSTERED 
(
	[RecruiterClassStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Enumerat__D6D1522791F3D8F2]    Script Date: 5/17/2018 10:36:22 AM ******/
ALTER TABLE [dbo].[EnumerationCategory] ADD UNIQUE NONCLUSTERED 
(
	[EnumTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [fk_Address_AddressTypeEnum_Enumeration_EnumId] FOREIGN KEY([AddressTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [fk_Address_AddressTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Address]  WITH NOCHECK ADD  CONSTRAINT [fk_Address_CountryEnum_Enumeration_EnumId] FOREIGN KEY([CountryEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Address] NOCHECK CONSTRAINT [fk_Address_CountryEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [fk_Address_CountyTypeEnum_Enumeration_EnumId] FOREIGN KEY([CountyEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [fk_Address_CountyTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [fk_Address_StateEnum_Enumeration_EnumId] FOREIGN KEY([StateEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [fk_Address_StateEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [fk_Address_ZipCodeEnum_Enumeration_EnumId] FOREIGN KEY([ZipCodeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [fk_Address_ZipCodeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_12thDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([12thDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_12thDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_12thDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([12thDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_12thDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_1stDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([1stDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_1stDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_1stDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([1stDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_1stDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_1stDistPrefChoice_Organization_OrganizationId] FOREIGN KEY([1stDistPrefChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_1stDistPrefChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_2ndDistPrefChoice_Organization_OrganizationId] FOREIGN KEY([2ndDistPrefChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_2ndDistPrefChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_3rdDistPrefChoice_Organization_OrganizationId] FOREIGN KEY([3rdDistPrefChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_3rdDistPrefChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_4thDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([4thDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_4thDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_4thDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([4thDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_4thDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_6thDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([6thDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_6thDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_6thDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([6thDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_6thDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_8thDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([8thDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_8thDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_8thDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([8thDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_8thDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_9thDist1stRSChoice_Organization_OrganizationId] FOREIGN KEY([9thDist1stRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_9thDist1stRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_9thDist2ndRSChoice_Organization_OrganizationId] FOREIGN KEY([9thDist2ndRSChoice])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_9thDist2ndRSChoice_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_BackgroundEnum_Enumeration_EnumId] FOREIGN KEY([BackgroundEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_BackgroundEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_HometownStateEnum_Enumeration_EnumId] FOREIGN KEY([HometownStateEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_HometownStateEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_HometownStateSpouseEnum_Enumeration_EnumId] FOREIGN KEY([HometownStateSpouseEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_HometownStateSpouseEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_OwnHomeStateEnum_Enumeration_EnumId] FOREIGN KEY([OwnHomeStateEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_OwnHomeStateEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_PresentFamilyLocStateEnum_Enumeration_EnumId] FOREIGN KEY([PresentFamilyLocStateEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_PresentFamilyLocStateEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_RecruiterId_Recruiter_RecruiterId] FOREIGN KEY([RecruiterId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_RecruiterId_Recruiter_RecruiterId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [fk_AssignmentSheet8411_SpouseRaceEnum_Enumeration_EnumId] FOREIGN KEY([SpouseRaceEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [fk_AssignmentSheet8411_SpouseRaceEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Asvab]  WITH CHECK ADD  CONSTRAINT [fk_ASVAB_PersonId_Person_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
GO
ALTER TABLE [dbo].[Asvab] CHECK CONSTRAINT [fk_ASVAB_PersonId_Person_PersonId]
GO
ALTER TABLE [dbo].[Asvab]  WITH CHECK ADD  CONSTRAINT [fk_ASVAB_VersionEnum_Enumeration_EnumId] FOREIGN KEY([VersionEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Asvab] CHECK CONSTRAINT [fk_ASVAB_VersionEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Billet]  WITH CHECK ADD  CONSTRAINT [fk_Billet_MOSEnum_Enumeration_EnumId] FOREIGN KEY([MosEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Billet] CHECK CONSTRAINT [fk_Billet_MOSEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Billet]  WITH CHECK ADD  CONSTRAINT [fk_Billet_OrganizationId_Organization_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[Billet] CHECK CONSTRAINT [fk_Billet_OrganizationId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[Billet]  WITH NOCHECK ADD  CONSTRAINT [fk_Billet_SectionEnum_Enumeration_EnumId] FOREIGN KEY([SectionEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Billet] NOCHECK CONSTRAINT [fk_Billet_SectionEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[BilletHistoryCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletHistoryCriteria_BilletId_Billet_BilletId] FOREIGN KEY([BilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[BilletHistoryCriteria] CHECK CONSTRAINT [fk_BilletHistoryCriteria_BilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[BilletHistoryCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletHistoryCriteria_RequiredBilletId_Billet_BilletId] FOREIGN KEY([RequiredBilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[BilletHistoryCriteria] CHECK CONSTRAINT [fk_BilletHistoryCriteria_RequiredBilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[BilletSpecialCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletSpecialCriteria_BilletId_Billet_BilletId] FOREIGN KEY([BilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[BilletSpecialCriteria] CHECK CONSTRAINT [fk_BilletSpecialCriteria_BilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[BilletSpecialCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletSpecialCriteria_EthnicityEnum_Enumeration_EnumId] FOREIGN KEY([EthnicityEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[BilletSpecialCriteria] CHECK CONSTRAINT [fk_BilletSpecialCriteria_EthnicityEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[BilletSpecialCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletSpecialCriteria_LanguageEnum_Enumeration_EnumId] FOREIGN KEY([LanguageEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[BilletSpecialCriteria] CHECK CONSTRAINT [fk_BilletSpecialCriteria_LanguageEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[BilletSpecialCriteria]  WITH CHECK ADD  CONSTRAINT [fk_BilletSpecialCriteria_RaceEnum_Enumeration_EnumId] FOREIGN KEY([RaceEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[BilletSpecialCriteria] CHECK CONSTRAINT [fk_BilletSpecialCriteria_RaceEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[ByNameRequest]  WITH CHECK ADD  CONSTRAINT [fk_ByNameRequest_RequestedId_Recruiter_RecruiterId] FOREIGN KEY([RequestedId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[ByNameRequest] CHECK CONSTRAINT [fk_ByNameRequest_RequestedId_Recruiter_RecruiterId]
GO
ALTER TABLE [dbo].[ByNameRequest]  WITH CHECK ADD  CONSTRAINT [fk_ByNameRequest_RequesterId_Recruiter_RecruiterId] FOREIGN KEY([RequesterId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[ByNameRequest] CHECK CONSTRAINT [fk_ByNameRequest_RequesterId_Recruiter_RecruiterId]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [fk_Course_BilletSectionTypeEnum_Enumeration_EnumId] FOREIGN KEY([BilletSectionTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [fk_Course_BilletSectionTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [fk_Course_OrganizationTypeEnum_Enumeration_EnumId] FOREIGN KEY([OrganizationTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [fk_Course_OrganizationTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[CourseCatalog]  WITH CHECK ADD  CONSTRAINT [fk_CourseCatalog_BilletId_Billet_BilletId] FOREIGN KEY([BilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[CourseCatalog] CHECK CONSTRAINT [fk_CourseCatalog_BilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[CourseCatalog]  WITH CHECK ADD  CONSTRAINT [fk_CourseCatalog_CourseId_Course_CourseId] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([CourseId])
GO
ALTER TABLE [dbo].[CourseCatalog] CHECK CONSTRAINT [fk_CourseCatalog_CourseId_Course_CourseId]
GO
ALTER TABLE [dbo].[CourseCatalog]  WITH CHECK ADD  CONSTRAINT [fk_CourseCatalog_CourseTypeEnum_Enumeration_EnumId] FOREIGN KEY([CourseTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[CourseCatalog] CHECK CONSTRAINT [fk_CourseCatalog_CourseTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[CourseCatalog]  WITH CHECK ADD  CONSTRAINT [fk_CourseCatalog_OrganizationId_Organization_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[CourseCatalog] CHECK CONSTRAINT [fk_CourseCatalog_OrganizationId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[Dependent]  WITH CHECK ADD  CONSTRAINT [fk_Dependent_DependentTypeEnum_Enumeration_EnumId] FOREIGN KEY([DependentTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Dependent] CHECK CONSTRAINT [fk_Dependent_DependentTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Dependent]  WITH CHECK ADD  CONSTRAINT [fk_Dependent_PersonId_Person_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
GO
ALTER TABLE [dbo].[Dependent] CHECK CONSTRAINT [fk_Dependent_PersonId_Person_PersonId]
GO
ALTER TABLE [dbo].[Enumeration]  WITH CHECK ADD  CONSTRAINT [fk_Enumeration_EnumerationCategory] FOREIGN KEY([TypeId])
REFERENCES [dbo].[EnumerationCategory] ([EnumTypeId])
GO
ALTER TABLE [dbo].[Enumeration] CHECK CONSTRAINT [fk_Enumeration_EnumerationCategory]
GO
ALTER TABLE [dbo].[GroupInstructor]  WITH CHECK ADD  CONSTRAINT [fk_GroupInstructor_GroupId_GroupRoster_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[GroupRoster] ([GroupId])
GO
ALTER TABLE [dbo].[GroupInstructor] CHECK CONSTRAINT [fk_GroupInstructor_GroupId_GroupRoster_GroupId]
GO
ALTER TABLE [dbo].[GroupInstructor]  WITH CHECK ADD  CONSTRAINT [fk_GroupInstructor_InstructorId_Recruiter_Dodidn] FOREIGN KEY([InstructorId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[GroupInstructor] CHECK CONSTRAINT [fk_GroupInstructor_InstructorId_Recruiter_Dodidn]
GO
ALTER TABLE [dbo].[GroupRoster]  WITH CHECK ADD  CONSTRAINT [fk_GroupRoster_CourseCatalogId_CourseCatalog_CourseCatalogId] FOREIGN KEY([CourseCatalogId])
REFERENCES [dbo].[CourseCatalog] ([CourseCatalogId])
GO
ALTER TABLE [dbo].[GroupRoster] CHECK CONSTRAINT [fk_GroupRoster_CourseCatalogId_CourseCatalog_CourseCatalogId]
GO
ALTER TABLE [dbo].[MPM_HMRevisionHistory]  WITH CHECK ADD  CONSTRAINT [fk_MPM_HMRevisionHistory_helId_MPM_HMRoles_helpId] FOREIGN KEY([helpId])
REFERENCES [dbo].[MPM_HelpMaterials] ([helpId])
GO
ALTER TABLE [dbo].[MPM_HMRevisionHistory] CHECK CONSTRAINT [fk_MPM_HMRevisionHistory_helId_MPM_HMRoles_helpId]
GO
ALTER TABLE [dbo].[MPM_HMRoles]  WITH CHECK ADD  CONSTRAINT [fk_MPM_HMRoles_helId_MPM_HMRoles_helpId] FOREIGN KEY([helpId])
REFERENCES [dbo].[MPM_HelpMaterials] ([helpId])
GO
ALTER TABLE [dbo].[MPM_HMRoles] CHECK CONSTRAINT [fk_MPM_HMRoles_helId_MPM_HMRoles_helpId]
GO
ALTER TABLE [dbo].[Organization]  WITH NOCHECK ADD  CONSTRAINT [fk_Organization_MEPSOrganizationId_Organization_OrganizationId] FOREIGN KEY([MepsOrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[Organization] NOCHECK CONSTRAINT [fk_Organization_MEPSOrganizationId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[Organization]  WITH CHECK ADD  CONSTRAINT [fk_Organization_OrganizationTypeEnum_Enumeration_EnumId] FOREIGN KEY([OrganizationTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Organization] CHECK CONSTRAINT [fk_Organization_OrganizationTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Organization]  WITH NOCHECK ADD  CONSTRAINT [fk_Organization_ParentId_Organization_OrganizationId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[Organization] NOCHECK CONSTRAINT [fk_Organization_ParentId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[OrganizationAddress]  WITH CHECK ADD  CONSTRAINT [fk_OrganizationAddress_AddressId_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[OrganizationAddress] CHECK CONSTRAINT [fk_OrganizationAddress_AddressId_Address_AddressId]
GO
ALTER TABLE [dbo].[OrganizationAddress]  WITH CHECK ADD  CONSTRAINT [fk_OrganizationAddress_OrganizationId_Organization_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[OrganizationAddress] CHECK CONSTRAINT [fk_OrganizationAddress_OrganizationId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [fk_Person_DriverLicStateEnum_Enumeration_EnumId] FOREIGN KEY([DriverLicStateEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [fk_Person_DriverLicStateEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [fk_Person_EthnicityEnum_Enumeration_EnumId] FOREIGN KEY([EthnicityEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [fk_Person_EthnicityEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [fk_Person_MaritalEnum_Enumeration_EnumId] FOREIGN KEY([MaritalEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [fk_Person_MaritalEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [fk_Person_PersonTypeEnum_Enumeration_EnumId] FOREIGN KEY([PersonTypeEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [fk_Person_PersonTypeEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [fk_Person_RaceEnum_Enumeration_EnumId] FOREIGN KEY([RaceEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [fk_Person_RaceEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[PersonAddress]  WITH CHECK ADD  CONSTRAINT [fk_PersonAddress_AddressId_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[PersonAddress] CHECK CONSTRAINT [fk_PersonAddress_AddressId_Address_AddressId]
GO
ALTER TABLE [dbo].[PersonAddress]  WITH CHECK ADD  CONSTRAINT [fk_PersonAddress_PersonId_Person_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
GO
ALTER TABLE [dbo].[PersonAddress] CHECK CONSTRAINT [fk_PersonAddress_PersonId_Person_PersonId]
GO
ALTER TABLE [dbo].[PersonLanguage]  WITH CHECK ADD  CONSTRAINT [fk_PersonLanguage_LanguageEnum_Enumeration_EnumId] FOREIGN KEY([LanguageEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[PersonLanguage] CHECK CONSTRAINT [fk_PersonLanguage_LanguageEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[PersonLanguage]  WITH CHECK ADD  CONSTRAINT [fk_PersonLanguage_PersonId_Person_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
GO
ALTER TABLE [dbo].[PersonLanguage] CHECK CONSTRAINT [fk_PersonLanguage_PersonId_Person_PersonId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_AciveDutySpouseBranchEnum_Enumeration_EnumId] FOREIGN KEY([AciveDutySpouseBranchEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_AciveDutySpouseBranchEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_BilletId_Billet_BilletId] FOREIGN KEY([BilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_BilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_MOSEnum_Enumeration_EnumId] FOREIGN KEY([MosEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_MOSEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_OrganizationId_Organization_OrganizationId] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_OrganizationId_Organization_OrganizationId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_PersonId_Person_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_PersonId_Person_PersonId]
GO
ALTER TABLE [dbo].[Recruiter]  WITH CHECK ADD  CONSTRAINT [fk_Recruiter_RankEnum_Enumeration_EnumId] FOREIGN KEY([RankEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[Recruiter] CHECK CONSTRAINT [fk_Recruiter_RankEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[RecruiterBilletHistory]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterBilletHistory_BilletId_Billet_BilletId] FOREIGN KEY([BilletId])
REFERENCES [dbo].[Billet] ([BilletId])
GO
ALTER TABLE [dbo].[RecruiterBilletHistory] CHECK CONSTRAINT [fk_RecruiterBilletHistory_BilletId_Billet_BilletId]
GO
ALTER TABLE [dbo].[RecruiterBilletHistory]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterBilletHistory_RecruiterId_Recruiter_Dodidn] FOREIGN KEY([RecruiterId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[RecruiterBilletHistory] CHECK CONSTRAINT [fk_RecruiterBilletHistory_RecruiterId_Recruiter_Dodidn]
GO
ALTER TABLE [dbo].[RecruiterBilletHistory]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterBilletHistory_RecruiterId_Recruiter_RecruiterId] FOREIGN KEY([RecruiterId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[RecruiterBilletHistory] CHECK CONSTRAINT [fk_RecruiterBilletHistory_RecruiterId_Recruiter_RecruiterId]
GO
ALTER TABLE [dbo].[RecruiterClassStatus]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterClassStatus_DropReasonEnum_Enumeration_EnumId] FOREIGN KEY([DropReasonEnum])
REFERENCES [dbo].[Enumeration] ([EnumId])
GO
ALTER TABLE [dbo].[RecruiterClassStatus] CHECK CONSTRAINT [fk_RecruiterClassStatus_DropReasonEnum_Enumeration_EnumId]
GO
ALTER TABLE [dbo].[RecruiterClassStatus]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterClassStatus_GroupId_GroupRoster_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[GroupRoster] ([GroupId])
GO
ALTER TABLE [dbo].[RecruiterClassStatus] CHECK CONSTRAINT [fk_RecruiterClassStatus_GroupId_GroupRoster_GroupId]
GO
ALTER TABLE [dbo].[RecruiterClassStatus]  WITH CHECK ADD  CONSTRAINT [fk_RecruiterClassStatus_RecruiterId_Recruiter_RecruiterId] FOREIGN KEY([RecruiterId])
REFERENCES [dbo].[Recruiter] ([RecruiterId])
GO
ALTER TABLE [dbo].[RecruiterClassStatus] CHECK CONSTRAINT [fk_RecruiterClassStatus_RecruiterId_Recruiter_RecruiterId]
GO
ALTER TABLE [dbo].[AssignmentSheet8411]  WITH CHECK ADD  CONSTRAINT [CK_AssignmentSheet8411_SpouseRaceEnum] CHECK  (([MarriageInterracial]=(1) AND [SpouseRaceEnum] IS NOT NULL))
GO
ALTER TABLE [dbo].[AssignmentSheet8411] CHECK CONSTRAINT [CK_AssignmentSheet8411_SpouseRaceEnum]
GO
ALTER TABLE [dbo].[BilletSpecialCriteria]  WITH CHECK ADD  CONSTRAINT [CK_BilletSpecialCriteria_NotAllNull] CHECK  (([RaceEnum] IS NOT NULL AND [EthnicityEnum] IS NOT NULL AND [LanguageEnum] IS NOT NULL))
GO
ALTER TABLE [dbo].[BilletSpecialCriteria] CHECK CONSTRAINT [CK_BilletSpecialCriteria_NotAllNull]
GO
